//
//  CompressedVC.swift
//  image_compression
//
//  Created by Narci on 18/10/2019.
//  Copyright © 2019 Narci. All rights reserved.
//

import UIKit

class CompressedVC: UIViewController {

    let URLname = "Https//urlfittizio.com/upload"
    let filename = "prova.jpg"
    
    @IBOutlet weak var reductionLabel: UILabel!
    @IBOutlet weak var myCompressedImage: UIImageView!
    var receivedImage: UIImage!
    var originalImage: UIImage!
    var reduction: Float!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myCompressedImage.image = receivedImage
        reductionLabel.text = "Bytes reduction \(reduction!.rounded())%"
    }
    
    @IBAction func sendToUrlButton(_ sender: Any) {
        var urlRequest = URLRequest(url: URL(fileURLWithPath: URLname))
        urlRequest.httpMethod = "POST"
        var data = Data()
        let boundary = UUID().uuidString
        
        let fieldName = "reqtype"
        let fieldValue = "fileupload"
        let fieldName2 = "userhash"
        let fieldValue2 = "caa3dce4fcb36cfdf9258ad9c"
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(fieldName)\"\r\n\r\n".data(using: .utf8)!)
        data.append("\(fieldValue)".data(using: .utf8)!)
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(fieldName2)\"\r\n\r\n".data(using: .utf8)!)
        data.append("\(fieldValue2)".data(using: .utf8)!)
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"fileToUpload\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(originalImage.jpegData(compressionQuality: 0.55)!)
        print(data)

        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
            
            if(error != nil){
                print("\(error!.localizedDescription)")
            }
            guard let responseData = responseData else {
                print("no response")
//                probabile risultato da un url fittizio
                return
            }
            if let responseString = String(data: responseData, encoding: .utf8) {
                print("Inviata: \(responseString)")
//                non è possibile con url fittizio
            }
        }).resume()
    }
    

}
