//
//  ViewController.swift
//  image_compression
//
//  Created by Narci on 18/10/2019.
//  Copyright © 2019 Narci. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var compressButton: UIButton!
    let image = UIImagePickerController()
    
    @IBOutlet weak var myImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        compressButton.isHidden = true
    }
    @IBAction func newPhotoButton(_ sender: Any) {
        presentImagePicker()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            let testImage = image
            self.myImage.image = testImage
            self.myImage.contentMode = .scaleAspectFit
            self.compressButton.isHidden = false
        }else{
            print("error loading photo")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func presentImagePicker() {
        let imagePickerActionSheet = UIAlertController(title: "Get Image",
                                                       message: nil, preferredStyle: .actionSheet)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraButton = UIAlertAction(title: "Take Photo",
                                             style: .default) { (alert) -> Void in
                                                let imagePicker = UIImagePickerController()
                                                imagePicker.delegate = self
                                                imagePicker.sourceType = .camera
                                                self.present(imagePicker, animated: true)
            }
            imagePickerActionSheet.addAction(cameraButton)
        }
        
        let libraryButton = UIAlertAction(title: "Choose Existing",
                                          style: .default) { (alert) -> Void in
                                            let imagePicker = UIImagePickerController()
                                            imagePicker.delegate = self
                                            imagePicker.sourceType = .photoLibrary
                                            self.present(imagePicker, animated: true)
        }
        imagePickerActionSheet.addAction(libraryButton)

        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        imagePickerActionSheet.addAction(cancelButton)
        present(imagePickerActionSheet, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let (compressedImage,reduction) = compress()
        let destination = segue.destination
        if let vc = destination as? CompressedVC {
            vc.receivedImage = compressedImage
            vc.originalImage = myImage.image
            vc.reduction = reduction
        }
    }
    
    
    func compress()->(UIImage,Float){
        let jpegData = myImage.image!.jpegData(compressionQuality: 0.55)!
        let jpegData2 = myImage.image!.jpegData(compressionQuality: 1)!
        print(jpegData)
        print(jpegData2)
//        jpeg.jpegtran
        return (UIImage(data: jpegData)!,(Float(jpegData.count)/Float(jpegData2.count)) * 100)
        
    }


}

